FROM python
WORKDIR /app
ADD app.py /app/
CMD ["python", "app.py"]