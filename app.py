from flask import Flask, request, jsonify
import sqlite3
import os



app = Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return ""


@app.route('/health', methods=['GET'])
def health():
    return jsonify("OK")


@app.route('/debug')
def debug():
   return request.query_string


@app.route('/api/v1/stocks')
def get():
    ticker = request.args.get('ticker')
    conn = sqlite3.connect('example.db')
    c = conn.cursor()
    t = (ticker,)
    c.execute('SELECT * FROM stocks WHERE symbol=?;', t)
    value = c.fetchall()
    if len(value) == 0:
        return jsonify("No record found")
    else:
        return jsonify(value)


def dbsetup():
    try:
        os.remove('example.db')
    except FileNotFoundError:
        print("no database found")

    conn = sqlite3.connect('example.db')
    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE stocks
                 (date text, trans text, symbol text, qty real, price real)''')

    # Insert a row of data
    c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

    # Save (commit) the changes
    conn.commit()

    # We can also close the connection if we are done with it.
    # Just be sure any changes have been committed or they will be lost.
    conn.close()

dbsetup()
app.run()
